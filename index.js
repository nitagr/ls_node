const fs = require('fs');
const path = require('path');

// Yellow shows its a directory and white for a file

// run the command:  node -e 'require(pathToindexFile).init(__dirname)'  in the console
module.exports.init = (dirPath) => {
    fs.readdir(dirPath, {withFileTypes: true}, (err, files) => {
        if (err) {
            return console.log('Unable to Process: ' + err);
        }
        files.map( item => {
            if(item.isDirectory()) {
                console.log("\x1b[33m%s\x1b[0m", item.name);
            } else {
                console.log(item.name);
            }
        });
    });
}
 

